from datetime import datetime
from django.contrib.sitemaps import Sitemap

class BasicSitemap(Sitemap):
    changefreq="always"
    priority=0.5
    
    def items(self):
        return Object.objects.filter(publish__gte=datetime.now())
    
    def lastmod(self, obj):
        return obj.publish
