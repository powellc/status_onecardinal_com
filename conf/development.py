from common import *

DEBUG=True
TESTING=False
TEMPLATE_DEBUG = DEBUG

import os

DATABASES = {
    'default': {
        'NAME':'test.db',
        'ENGINE':'django.db.backends.sqlite3',
    },
}

MEDIA_URL='http://127.0.0.1:8000/media/'
MEDIA_ROOT= os.path.join(PROJECT_ROOT, 'media/')

STATIC_URL='/static/'
STATIC_ROOT= os.path.join(PROJECT_ROOT, 'static/')

ADMIN_MEDIA_PREFIX=STATIC_URL+'admin/'

TEMPLATE_DIRS = (os.path.join(PROJECT_ROOT, 'templates'),)

INTERNAL_IPS = ('24.93.128.34','10.1.1.23','127.0.0.1' )

#Start up logging to the console

logging.getLogger('').addHandler(CONSOLE_HANDLER)
logging.debug("Django Started")

from datetime import timedelta
ISITUP_STATUS_INTERVAL=timedelta(minutes=2)

#RECAPTCHA_PUBLIC_KEY = ''
#RECAPTCHA_PRIVATE_KEY = ''

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
)

DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
}

CACHE_BACKEND='cache_utils.group_backend://localhost:11211/'
CACHE_TIMEOUT = 60*5

ACCOUNT_ACTIVATION_DAYS=15
EMAIL_HOST="smtp.gmail.com"
EMAIL_PORT=587
EMAIL_HOST_USER="colin.powell@gmail.com"
EMAIL_HOST_PASSWORD="hall6-jerkin"
DEFAULT_FROM_EMAIL="colin.powell@gmail.com"
EMAIL_USE_TLS = True
