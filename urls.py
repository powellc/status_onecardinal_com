from django.conf.urls.defaults import *
from django.conf import settings
from django.views.static import serve
from unipath import FSPath as Path
from django.views.generic import DetailView, ListView
#from mydomain_com.sitemap import BasicSitemap 

#feeds = {
#   'events': UpcomingEvents,
#   'news': LatestNews,
#}

#sitemaps={
#    'site':BasicSitemap,
#}

if settings.DEBUG:
    urlpatterns = patterns('', 
        (r'^media/(?P<path>.*)$', serve, 
			{'document_root' : Path(__file__).parent.child("media")}),
    )
else:
    urlpatterns = patterns('',)

from django.contrib import admin
admin.autodiscover()

urlpatterns += patterns('',
    (r'^accounts/', include('registration.urls')),
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),
    (r'^admin/', include(admin.site.urls)),
    (r'^', include('isitup.urls')),
)

urlpatterns+= patterns('django.contrib.flatpages.views',
    url(r'^example/$', 'flatpage', {'url': '/example/'}, name='fl-example'),
)
